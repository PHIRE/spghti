.. spghti documentation master file. it should at least
   contain the root `toctree` directive.

SPGH Immunity Task Force, Data Mgmt
===================================

Documentation surronding the SPHG immunity task force. It includes how to contribute new data, how to
request existing data, and how to explore available data offerings.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   lifecycle/overview
   extra/example

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
