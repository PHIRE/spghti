Process Overview
================

**Living Description of the Lifecycle of a New Study**

1. A study team contacts SPGHTI team with data schema and dictionary.
2. Using the schema and dictionary, we define a view on the study data that will integrate with other datasets in a semi harmonized way.
3. Documentation begins regarding the team, the collection process, the study, and the population.
4. Documentation continues with the data elements to be loaded.
5. Loading scripts are created to load the dataset given in DBMS system.
6. Connection is made between Opal and the DBMS tables for this study.
7. Dataset is presented in the consortium view, connected in Mica.


The purpose of having an accurate process description is to plan future effort and required collaborations.