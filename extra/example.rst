reStructured Text Cheat Sheet
=============================

If you want an actual RST CheatSheet_.

.. _CheatSheet: https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst

.. csv-table:: Frozen Delights!
   :header: "Treat", "Quantity", "Description"
   :widths: 15, 10, 30

   "Albatross", 2.99, "On a stick!"
   "Popcorn", 1.99, "Straight from the oven"


.. math:: (a + b)^2 = a^2 + 2ab + b^2

