How to edit this document
=========================

There is no WYSIWYG (What You See Is What You Get) editor for this document. It must be edited
in a text editor following the reStructured format (.rst).

Here is my suggestion:

1. Clone the repository_ locally.
2. Download_ and install Visual Studio Code (an editor)
3. Edit `.rst` files and push to the repo.
4. You may want to install plugins recommended by the editor to simplify writing in a correct syntax.

.. _repository: https://git.mchi.mcgill.ca/PHIRE/spghti
.. _Download: https://code.visualstudio.com/